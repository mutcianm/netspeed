CC=gcc
CFLAGS=-O2 -Wall
LDFLAGS=-lcurses

all: netspeed

netspeed: ns.c
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ ns.c

clean:
	rm -f netspeed
