#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/stat.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <ncurses.h>

#define OP_RXB 1
#define OP_TXB 2

#define NET_DIR "/sys/class/net/"

typedef struct 
{
    int summary;
    unsigned int elements;
    unsigned int delay;
    char** inerfaces;
    int iface_num;
} settings_t;

typedef struct
{
    unsigned int rate_rx_kBps;
    unsigned int rate_tx_kBps;
} rate_t;

typedef struct 
{
    char* name;
    unsigned int rx_bytes;
    unsigned int rx_delta;
    int rxb_fd;
    unsigned int tx_bytes;
    unsigned int tx_delta;
    int txb_fd;
    rate_t rate;
} iface_stat;

typedef struct
{
    settings_t settings;
    iface_stat* interfaces;
    int buffer_fd;
} state_t;

void
parse_settings(settings_t* settings, char** str, int argc)
{
    char buf[16][16];
    settings->elements = OP_RXB | OP_TXB;
    if(argc <= 1){
        DIR* d = opendir(NET_DIR);
        struct dirent* de;
        int i = 0;
        while((de = readdir(d)) != NULL) {
            if(!strcmp(de->d_name, ".") || !strcmp(de->d_name, "..")) continue;
            printf("found iface %s\n", de->d_name);
            strncpy(buf[i], de->d_name, 16);
            i = (i+1) % 16;
        }
        settings->iface_num = i;
        settings->inerfaces = (char**)calloc(settings->iface_num, sizeof(char**));
        for(i = 0; i < settings->iface_num; ++i) {
            settings->inerfaces[i] = strdup(buf[i]);
        }
    } else {
        settings->iface_num = argc - 1;
        settings->inerfaces = &str[1];
    }
    settings->delay = 1;
    settings->summary = 1;
}

void
setup_interfaces(state_t* state)
{
    state->interfaces = (iface_stat*)calloc(state->settings.iface_num, sizeof(iface_stat));
    int i;
    for(i = 0; i < state->settings.iface_num; ++i) {
        memset(&state->interfaces[i], 0, sizeof(iface_stat));
        state->interfaces[i].name = state->settings.inerfaces[i];
        char buf[256];
        if(state->settings.elements & OP_RXB){
            sprintf(buf, "/sys/class/net/%s/statistics/rx_bytes",
                    state->interfaces[i].name);
            state->interfaces[i].rxb_fd = open(buf, O_RDONLY);
        }
        if(state->settings.elements & OP_TXB){
            sprintf(buf, "/sys/class/net/%s/statistics/tx_bytes",
                    state->interfaces[i].name);
            state->interfaces[i].txb_fd = open(buf, O_RDONLY);
        }
    }
}

void
fill_iface_stat(iface_stat* stat, settings_t* settings)
{
    char buf[64];
    if(settings->elements & OP_RXB){
        bzero(buf, 64);
        lseek(stat->rxb_fd, 0, SEEK_SET);
        read(stat->rxb_fd, buf, 640);
        unsigned int num = atoi(buf);
        stat->rx_delta = num - stat->rx_bytes;
        stat->rx_bytes = num;
    }
    if(settings->elements & OP_TXB){
        bzero(buf, 64);
        lseek(stat->txb_fd, 0, SEEK_SET);
        read(stat->txb_fd, buf, 640);
        unsigned int num = atoi(buf);
        stat->tx_delta = num - stat->tx_bytes;
        stat->tx_bytes = num;
    }
}

void
compute_iface_rate(iface_stat* stat, settings_t* settings)
{
    stat->rate.rate_rx_kBps = (stat->rx_delta / 1024) / settings->delay;
    stat->rate.rate_tx_kBps = (stat->tx_delta / 1024) / settings->delay;
}

void
print_info(state_t* state)
{
    erase();
    int i;
    for(i = 0; i < state->settings.iface_num; ++i){
        iface_stat* s = &state->interfaces[i];
        printw("%s\t[ RX: %d KB/s, TX: %d KB/s ]\n", s->name, s->rate.rate_rx_kBps, s->rate.rate_tx_kBps);
    }
    refresh();
}

int 
main(int argc, char** argv)
{
    state_t st;
    parse_settings(&st.settings, argv, argc);
    setup_interfaces(&st);
    initscr();
    while(1)
    {
        int i;
        for(i = 0; i < st.settings.iface_num; ++i){
            fill_iface_stat(&st.interfaces[i], &st.settings);
            compute_iface_rate(&st.interfaces[i], &st.settings);
        }
        print_info(&st);
        sleep(st.settings.delay);
    }
    endwin();
    return 0;
}
